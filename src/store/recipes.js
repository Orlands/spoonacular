/**
 * Created by Orlands on 20/10/2020.
 */

import axios from 'axios'

const recipes = {
  state: () => ({
    randomRecipe: {},
    recipeId: '',
    recipeInfo: {}
  }),
  mutations: {
    getRecipe (state, data) {
      state.randomRecipe = data
    },
    setRecipeInfo (state, data) {
      state.recipeIngo = data
    }
  },
  actions: {
    getRandomRecipe ({ commit }) {
      axios.get('https://api.spoonacular.com/recipes/random', {
        params: {
          apiKey: '029d2e1ed80f471a9f300fb2a365717a',
          number: 14
        }
      }).then(response => {
        console.log(response)
        commit('getRecipe', response.data.recipes)
      })
    },
    getRecipeInfo ({ commit }, params) {
      axios.get('https://api.spoonacular.com/recipes/' + params.id + '/information', {
        params: {
          apiKey: '029d2e1ed80f471a9f300fb2a365717a',
          number: 14
        }
      }).then(response => {
        console.log(response)
        commit('setRecipeInfo', response)
      })
    }
  },
  getters: {

  }
}

export default recipes
